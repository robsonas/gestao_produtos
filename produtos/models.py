from django.db import models

# Create your models here.

class Produto(models.Model):
    """Model definition for Produto."""
    # TODO: Define fields here

    UNIDADE_CHOICES = (
        ('pc','Peça'),
        ('gr','Gramas'),
        ('kg','Kilograma'),
        ('cm','Centímetros'),
        ('mt','Metros'),
    )

    nome = models.CharField(max_length=30)
    fabricante = models.CharField(max_length=30)
    distribuidor = models.CharField(max_length=30)
    medida = models.DecimalField(max_digits=6, decimal_places=2)
    unidade_produto = models.CharField(max_length=2, choices=UNIDADE_CHOICES, default='pc')
    preco_unitario = models.DecimalField(max_digits=10, decimal_places=2)
    qtd_estoque = models.IntegerField()
    descricao = models.TextField()
    foto = models.ImageField(upload_to='fotos_produtos', null=True, blank=True)

    def __str__(self):
        """Unicode representation of Produto."""
        return self.nome
