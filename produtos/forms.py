from django import forms
from .models import Produto


class ProdutoForm(forms.ModelForm):
    """Form definition for Produto."""

    class Meta:
        """Meta definition for Produtoform."""

        model = Produto
        fields = [
            'nome',
            'fabricante',
            'distribuidor',
            'medida',
            'unidade_produto',
            'preco_unitario',
            'qtd_estoque',
            'descricao',
            'foto'
            ]
