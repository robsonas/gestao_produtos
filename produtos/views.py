from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from produtos.models import Produto
from produtos.forms import ProdutoForm

# Create your views here.

@login_required  # permite o acesso mediante autenticação somente
def lista_produtos(request):
    nome = request.GET.get('nome', None)
    fabricante = request.GET.get('fabricante', None)
    produtos = None

    if nome or fabricante:
        # lógica 'AND'
        # produtos = Produto.objects.filter(nome__icontains=nome, fabricante__icontains=fabricante)
        # lógica 'OR'
        produtos = Produto.objects.filter(nome__icontains=nome) | Produto.objects.filter(fabricante__icontains=fabricante)
    else:
        produtos = Produto.objects.all()
    # aqui foi utilizado uma forma de proteger o nome dos templates 
    # por repetir o nome do app dentro de cada template
    return render(request, 'produtos/lista.html', {'produtos': produtos})


@login_required  # permite o acesso mediante autenticação somente
def novo_produto(request):
    form = ProdutoForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect('lista_produtos')
    return render(request, 'produtos/novo_produto.html', {'form': form})


# @login_required  # permite o acesso mediante autenticação somente
# def atualiza_produto(request, id):    
#     produto = get_object_or_404(Produto, pk=id)    
#     form = ProdutoForm(request.POST or None, request.FILES or None, instance=produto)
#     if form.is_valid():
#         form.save()
#         return redirect('lista_produtos')        
#     return render(request, 'produtos/atualiza_produto.html', {'form': form})


@login_required  # permite o acesso mediante autenticação somente
def atualiza_produto(request, id):
    produto = get_object_or_404(Produto, pk=id)
    if request.method == 'POST':
        form = ProdutoForm(request.POST, request.FILES, instance=produto)
        if form.is_valid():
            produto = form.save(commit=False)
            produto.save()
            return redirect('lista_produtos')
    else:
        form = ProdutoForm(instance=produto)
    return render(request, 'produtos/atualiza_produto.html', {'form': form})   


@login_required  # permite o acesso mediante autenticação somente
def exclui_produto(request, id):
    produto = get_object_or_404(Produto, pk=id)
    if request.method == 'POST':
        produto.delete()
        return redirect('lista_produtos')    
    return render(request, 'produtos/exclui.html', {'produto': produto})
