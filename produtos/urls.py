from django.urls import path
from produtos.views import lista_produtos, novo_produto, atualiza_produto, exclui_produto


urlpatterns = [
    path('lista/', lista_produtos, name='lista_produtos'),
    path('novo/', novo_produto, name='novo_produto'),
    path('atualiza/<int:id>/', atualiza_produto, name='atualiza_produto'),
    path('exclui/<int:id>/', exclui_produto, name='exclui_produto'),
]
