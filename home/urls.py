from django.urls import path
from home.views import home_view, logout_view


urlpatterns = [
    path('', home_view, name="home_view"),    
    path('logout/', logout_view, name="logout_view"),    
]
